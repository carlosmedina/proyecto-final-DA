﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DALed.Models;
using Leds.Controllers;

namespace DALed
{
    public partial class PrendeApagarLeds : Form
    {
        private string estado1 { get; set; }
        private string estado2 { get; set; }
        private string estado3 { get; set; }

        public int IdUsuarioLog { get; set; }

        public int rol { get; set; }

        public PrendeApagarLeds()
        {
            InitializeComponent();
        }

        private void PrendeApagarLeds_Load(object sender, EventArgs e)
        {
            IdUsuarioLog = this.IdUsuarioLog;
            rol = this.rol;
            estado1 = "Apagado Led1";
            estado2 = "Apagado Led2";
            estado3 = "Apagado Led3";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var controlador = new ControladorLogin();
            var usuario = controlador.UsuarioInfo(IdUsuarioLog);
            var usuarioLogInformacion = new Logs();

            usuarioLogInformacion.NombreUsuario = usuario.nombreUsuario;            
            usuarioLogInformacion.fechaEntrada = DateTime.Now;
            usuarioLogInformacion.fkUsuario = usuario.idUsuario;            
            usuarioLogInformacion.accion = prenderLed1(usuarioLogInformacion.accion);

            controlador.GuardarLogs(usuarioLogInformacion);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var controlador = new ControladorLogin();
            var usuario = controlador.UsuarioInfo(IdUsuarioLog);
            var usuarioLogInformacion = new Logs();

            usuarioLogInformacion.NombreUsuario = usuario.nombreUsuario;
            usuarioLogInformacion.fechaEntrada = DateTime.Now;
            usuarioLogInformacion.fkUsuario = usuario.idUsuario;
            usuarioLogInformacion.accion = prenderLed2(usuarioLogInformacion.accion);

            controlador.GuardarLogs(usuarioLogInformacion);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var controlador = new ControladorLogin();
            var usuario = controlador.UsuarioInfo(IdUsuarioLog);
            var usuarioLogInformacion = new Logs();

            usuarioLogInformacion.NombreUsuario = usuario.nombreUsuario;
            usuarioLogInformacion.fechaEntrada = DateTime.Now;
            usuarioLogInformacion.fkUsuario = usuario.idUsuario;
            usuarioLogInformacion.accion = prenderLed3(usuarioLogInformacion.accion);

            controlador.GuardarLogs(usuarioLogInformacion);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ListaDeUsuarios ventaListaDeUsuarios = new ListaDeUsuarios();
            ventaListaDeUsuarios.rol = rol;
            ventaListaDeUsuarios.IdUsuarioLog = IdUsuarioLog;
            this.Close();
            ventaListaDeUsuarios.Show();
        }

        public string prenderLed1(string accion)
        {

            if (estado1 == "Apagado Led1")
            {
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
                estado1 = "Prendido Led1";
                return estado1;
            }
            else
            {
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;
                estado1 = "Apagado Led1";
                return estado1;
            }
        }

        public string prenderLed2(string accion)
        {

            if (estado2 == "Apagado Led2")
            {
                pictureBox3.Visible = false;
                pictureBox4.Visible = true;
                estado2 = "Prendido Led2";
                return estado2;
            }
            else
            {
                pictureBox3.Visible = true;
                pictureBox4.Visible = false;
                estado2 = "Apagado Led2";
                return estado2;
            }
        }

        public string prenderLed3(string accion)
        {

            if (estado3 == "Apagado Led3")
            {
                pictureBox5.Visible = false;
                pictureBox6.Visible = true;
                estado3 = "Prendido Led3";
                return estado3;
            }
            else
            {
                pictureBox5.Visible = true;
                pictureBox6.Visible = false;
                estado3 = "Apagado Led3";
                return estado3;
            }
        }
    }
}
