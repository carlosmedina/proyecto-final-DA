﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DALed.Models;
using Leds.Controllers;

namespace DALed
{
    public partial class CrearUsuario : Form
    {
        public int IdUsuarioLog { get; set; }
        public int IdUsuario { get; set; }
        public string evento { get; set; }
        public int rol { get; set; }
        public CrearUsuario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                var usuario = new Usuario();
                var controladorUsuario = new ControladorLogin();

                if (textBox3.Text == textBox4.Text)
                {
                    if (radioButton1.Checked)
                    {
                        usuario.rol = 1;
                    }
                    if (radioButton2.Checked)
                    {
                        usuario.rol = 2;
                    }
                    usuario.correo = textBox2.Text;
                    usuario.contrasena = textBox3.Text;
                    usuario.contrasena = textBox4.Text;
                    usuario.nombreUsuario = textBox1.Text;
                    controladorUsuario.Guardar(usuario);

                    var ventanaLogin = new Inicio();
                    this.Close();
                    ventanaLogin.Show();
                }
                else
                {
                    MessageBox.Show("La contrasenas no coinciden");
                }
            }

            else
            {
                var usuario = new Usuario();
                var controladorUsuario = new ControladorLogin();               

                if (textBox3.Text == textBox4.Text)
                {
                    if (radioButton1.Checked)
                    {
                        usuario.rol = 1;
                    }
                    if (radioButton2.Checked)
                    {
                        usuario.rol = 2;
                    }

                    usuario = controladorUsuario.UsuarioInfo(IdUsuario);
                    usuario.correo = textBox2.Text;
                    usuario.contrasena = textBox3.Text;
                    usuario.contrasena = textBox4.Text;
                    usuario.nombreUsuario = textBox1.Text;
                    controladorUsuario.Modificar(usuario);

                    var ventanaListaDeUsuarios = new ListaDeUsuarios();
                    this.Close();
                    ventanaListaDeUsuarios.Show();
                }
                else
                {
                    MessageBox.Show("La contrasenas no coinciden");
                }
            }
           
        }       

        private void CrearUsuario_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            IdUsuarioLog = this.IdUsuarioLog;
            var idUsuario = this.IdUsuario;
            rol = this.rol;
            
            if (idUsuario > 0)
            {
                ControladorLogin controlador = new ControladorLogin();
                var usuario = controlador.UsuarioInfo(idUsuario);
                textBox1.Text = usuario.nombreUsuario;
                textBox2.Text = usuario.correo;
                textBox3.Text = usuario.contrasena;
                textBox4.Text = usuario.contrasena;   
                textBox5.Text = usuario.idUsuario.ToString();
                
                if (usuario.rol == 1)
                {
                    radioButton1.Checked = true;
                }
                if (usuario.rol == 2)
                {
                    radioButton2.Checked = true;
                }

                if (evento == "Eliminar")
                {
                    button1.Visible = false;
                    button2.Visible = true;
                }

                if (evento == "Modificar")
                {
                    button2.Visible = false;
                    button1.Visible = true;
                    button1.Text = "Modificar";
                }
            }           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var usuario = new Usuario();
            var controladorUsuario = new ControladorLogin();

            if (radioButton1.Checked)
            {
                usuario.rol = 1;
            }
            if (radioButton2.Checked)
            {
                usuario.rol = 2;
            }

            usuario = controladorUsuario.UsuarioInfo(IdUsuario);
            usuario.correo = textBox2.Text;
            usuario.contrasena = textBox3.Text;
            usuario.contrasena = textBox4.Text;
            usuario.nombreUsuario = textBox1.Text;
            controladorUsuario.Eliminar(usuario);

            var ventanaListaDeUsuarios = new ListaDeUsuarios();
            this.Close();
            ventanaListaDeUsuarios.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ListaDeUsuarios listaDeUsuarios = new ListaDeUsuarios();
            listaDeUsuarios.IdUsuarioLog = IdUsuarioLog;
            listaDeUsuarios.rol = rol;
            this.Close();
            listaDeUsuarios.Show();
        }
    }
}
