﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Leds.Controllers;

namespace DALed
{
    public partial class Inicio : Form
    {
        public int IdUsuarioLog { get; set; }
        public Inicio()
        {
            InitializeComponent();
        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ControladorLogin controlador = new ControladorLogin();
            var acceso = controlador.loginExisteUsuario(textBox1.Text, textBox2.Text);
            ListaDeUsuarios listaform = new ListaDeUsuarios();

            if (acceso)
            {
                MessageBox.Show("Accesso aprobado");
                this.Hide();
                listaform.IdUsuarioLog = controlador.IdUsuarioLog(textBox1.Text, textBox2.Text);
                listaform.rol = Convert.ToInt32(controlador.IdUsuario(textBox1.Text, textBox2.Text));
                listaform.Show();
            }

            else
            {
                MessageBox.Show("Acceso denegado");
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var ventanaUsuario = new CrearUsuario();
            ventanaUsuario.Show();
        }
    }
}
