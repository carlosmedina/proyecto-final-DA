﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALed.Models;

namespace Leds.Controllers
{
    public class ControladorLogin
    {
        private readonly DataClasses1DataContext _contexto;

        public ControladorLogin()
        {
        }

        public ControladorLogin(DataClasses1DataContext contexto)
        {
            this._contexto = contexto;
        }

        public object Guardar(Usuario guardar)
        {
            using (var dc = new DataClasses1DataContext())
            {
                dc.Usuario.InsertOnSubmit(guardar);

                dc.SubmitChanges();
            }
            return guardar;
        }

        public void GuardarLogs(Logs guardar)
            {
            using (var dc = new DataClasses1DataContext())
            {
                dc.Logs.InsertOnSubmit(guardar);

                dc.SubmitChanges();
            }
        }

        public object Modificar(Usuario modificar)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var actual = dc.Usuario.Single(f => f.idUsuario == modificar.idUsuario);

                actual.contrasena = modificar.contrasena;
                actual.nombreUsuario = modificar.nombreUsuario;
                actual.correo = modificar.correo;
                actual.rol = modificar.rol;
                dc.SubmitChanges();
            }

            return modificar;
        }

        public object Eliminar(Usuario eliminar)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var actual = dc.Usuario.Single(f => f.idUsuario == eliminar.idUsuario);

                actual.contrasena = eliminar.contrasena;
                actual.nombreUsuario = eliminar.nombreUsuario;
                actual.correo = eliminar.correo;
                actual.rol = eliminar.rol;
                dc.Usuario.DeleteOnSubmit(actual);
                dc.SubmitChanges();
            }
            return eliminar;
        }

        public bool loginExisteUsuario(string correo, string contrasena)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var id = 0;
                var query = dc.Usuario.Where(l => l.correo == correo && l.contrasena == contrasena);
                return id <= 0
                    ? query.Any()
                    : query.Any(l => l.idUsuario != id);
            }
        }

        public int IdUsuario(string correo, string contrasena)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var idusuario = (from e in dc.Usuario
                    where e.contrasena == contrasena && e.correo == correo
                    select e.rol).FirstOrDefault();

                return idusuario;
            }
        }

        public int IdUsuarioLog(string correo, string contrasena)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var idusuario = (from e in dc.Usuario
                    where e.contrasena == contrasena && e.correo == correo
                    select e.idUsuario).FirstOrDefault();

                return idusuario;
            }
        }

        public List<Logs> ListaAccionUsuarios()
        {
            using (var dc = new DataClasses1DataContext())
            {
                var usuarios = from e in dc.Logs
                    where e.NombreUsuario.Contains("")
                    select e;
                return usuarios.ToList();
            }
        }

        public string descifrarContrasena(string contrasena)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var desencriptar = (from e in dc.Usuario
                                 where e.contrasena == contrasena 
                                 select e.contrasena).FirstOrDefault();

                return desencriptar;
            }
        }

        public List<Usuario> ListaUsuarios()
        {
            using (var dc = new DataClasses1DataContext())
            {
                var usuarios = from e in dc.Usuario
                    where e.nombreUsuario.Contains("")
                    select e;
                return usuarios.ToList();
            }
        }

        public Usuario UsuarioInformacion()
        {
            using (var dc = new DataClasses1DataContext())
            {
                var usuarios = from e in dc.Usuario
                    where e.nombreUsuario.Contains("")
                    select e;
                return usuarios.FirstOrDefault();
            }
        }

        public Usuario UsuarioInfo(int idUsuario)
        {
            using (var dc = new DataClasses1DataContext())
            {
                var usuarios = from e in dc.Usuario
                    where e.idUsuario == idUsuario
                    select e;
                return usuarios.FirstOrDefault();
            }
        }
    }
}
