﻿using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class Prueba5
    {
        [TestMethod]
        public void eliminarUsuario()
        {
            var idUsuario = 2011;

            ControladorLogin controlador = new ControladorLogin();

            //Act
            var obtenerUsuario = controlador.UsuarioInfo(idUsuario);
            obtenerUsuario.contrasena = "123";
            obtenerUsuario.correo = "cmc";
            obtenerUsuario.nombreUsuario = "sisq";
            obtenerUsuario.rol = 1;
            var usuarioPrueba = controlador.Eliminar(obtenerUsuario);

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}