﻿using System;
using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class Prueba7
    {
        [TestMethod]
        public void ListaAccionDeUsuarios()
        {
            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.ListaAccionUsuarios();

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}
