﻿using System;
using DALed;
using DALed.Models;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct4
    {
        [TestMethod]
        public void ModificarUsuario()
        {
            var idUsuario = 2;           

            ControladorLogin controlador = new ControladorLogin();

            //Act
            var obtenerUsuario = controlador.UsuarioInfo(idUsuario);
            obtenerUsuario.contrasena = "123";
            obtenerUsuario.correo = "cmc";
            obtenerUsuario.nombreUsuario = "hugo";
            obtenerUsuario.rol = 1;
            var usuarioPrueba = controlador.Modificar(obtenerUsuario);

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}