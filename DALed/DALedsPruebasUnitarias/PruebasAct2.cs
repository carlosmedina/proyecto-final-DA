﻿using System;
using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct2
    {
        [TestMethod]
        public void listaUsuarios()
        {           
            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.ListaAccionUsuarios();

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}
