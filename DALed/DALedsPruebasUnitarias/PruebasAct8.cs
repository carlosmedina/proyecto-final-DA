﻿using System;
using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct8
    {
        [TestMethod]
        public void informacionUsuario()
        {
            var idUsuario = 2;
            ControladorLogin controlador = new ControladorLogin();

            //Act
           var usuarioPrueba = controlador.UsuarioInfo(idUsuario);

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }

        [TestMethod]
        public void prenderApagarLedsPrueba1()
        {
            var accion = "Apagar Led1";
            PrendeApagarLeds prendeApagarLeds = new PrendeApagarLeds();

            //Act
           var actividad = prendeApagarLeds.prenderLed2(accion);

            //Asset
            Assert.IsNotNull(actividad);
        }

        [TestMethod]
        public void prenderApagarLedsPrueba2()
        {
            var accion = "Apagar Led2";
            PrendeApagarLeds prendeApagarLeds = new PrendeApagarLeds();

            //Act
            var actividad = prendeApagarLeds.prenderLed3(accion);

            //Asset
            Assert.IsNotNull(actividad);
        }

        [TestMethod]
        public void prenderApagarLedsPrueba3()
        {
            var accion = "Apagar Led3";
            PrendeApagarLeds prendeApagarLeds = new PrendeApagarLeds();

            //Act
            var actividad = prendeApagarLeds.prenderLed1(accion);

            //Asset
            Assert.IsNotNull(actividad);
        }
    }
}
