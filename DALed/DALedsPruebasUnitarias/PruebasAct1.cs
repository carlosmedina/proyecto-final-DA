﻿using System;
using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct1
    {
        [TestMethod]
        public void loginUsuario()
        {
            var correo = "cmc";
            var contrasena = "1234";
            ControladorLogin controlador = new ControladorLogin();

            //Act
           var usuarioPrueba = controlador.loginExisteUsuario(correo, contrasena);

            //Asset
            Assert.IsTrue(usuarioPrueba);
        }
    }
}
